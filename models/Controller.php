<?php

/**
 * Objet qui régit la vue
 */
abstract class Controller{
    public function loadModel(string $model){
        // On va chercher le fichier correspondant au modèle souhaité
        require_once ROOT.'models/'.$model.'.php';
        // On crée une instance de ce modèle. Ainsi "Article" sera accessible par $this->Article
        $model = new $model();
        // On retourne la variable
        return $model;
    }

    public function render(string $file, $data= array()){
        extract($data);
        // On démarre le buffer de sortie
        ob_start();
        // On génère la vue
        require_once ROOT.'views/'.strtolower(get_class($this)).'/'.$file.'.php';
        // On stocke le contenu dans $content
        $content = ob_get_clean();
        // On fabrique le "template"
        require_once ROOT.'views/layout/base.php';
    }
}