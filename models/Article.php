<?php 

/**
 * Objet Article héritant de Model
 */
class Article extends Model{
    public function __construct(){
        $this->table = "article";
        $this->id = $_GET['id'];
        $this->getConnection();
    }
    // On crée une requête permettant d'afficher tous les articles
    public function getAll(){
        $sql="SELECT * FROM ".$this->table;
        try {
            $query = $this->_connexion->prepare($sql);
            $query->execute();
        } catch (Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }
        return $query->fetchAll();
    }

    // On crée un requête permettant de lister un articlee
    public function getOne(){
        try {
            $query = $this->_connexion->prepare("SELECT * FROM ".$this->table." WHERE id = :id");
            $query->execute(array(
            ':id' => $this->id
            ));
        } catch (Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }
        
        return $query->fetch();
    }
    public function create($photo = null){
        $sql="REPLACE INTO " .$this->table."(`id`, `title`, `content`, `picture`, `created_at`) VALUES (:id, :title, :content, :picture, NOW())";
        try{
            $query = $this->_connexion->prepare($sql);
            $query->execute(array(
                ':id' => $_POST['id'],
                ':title' => $_POST['title'],
                ':content' => $_POST['content'],
                ':picture' => $photo
                )
            );
        } catch (Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }
        
        return $query;
    }
    public function remove(){
        try{
            $query = $this->_connexion->prepare("DELETE FROM " .$this->table. " WHERE id = :id");
            $query->execute(array(
                ':id' => $this->id
            ));
        } catch (Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }
        return $query;
    }
}

