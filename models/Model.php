<?php

/**
 * Objet de connexion à la base de donnée
 */
abstract class Model{
    // On attribue les informations de la bdd pour s'y connecter
    private $host='localhost';
    private $dbname='edelweissmagazine';
    private $user='root';
    private $password='';

    protected $_connexion;

    public $table;
    public $id;

    //ouvre une connexion à la bdd
    public function getConnection(){
        try{
            // On instancie un nouvel objet de la clase PDO
            $this->_connexion = new PDO(
                'mysql:host='. $this->host.';
                dbname='.$this->dbname,
                $this->user,
                $this->password,
                array(
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING, 
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                )
            );
        } catch(PDOException $e){
            print "Erreur :" . $e->getMessage() . "<br/>";
            die();
        }
    }
}