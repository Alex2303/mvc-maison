<?php 

// On définit la constante ROOT. Pour cela, on remplace index.php, le nom du dossier dans lequel on se trouve, par ''

define('ROOT', str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']));
define('SCRIPT_ROOT', 'http://localhost/edelweissmagazine/');

// Fonction qui permet d'afficher plus proprement les variables
function debug($var){
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}

function isLogged() {
    if(isset($_SESSION['pseudo'])) {
        return true;
    } else {
        return false;
    }
}

function autoload($folder, $className){
    require_once($folder. '/'.$className.'.php');
}

function redirectWithError($error)
{
    $_SESSION['_contact_form_error'] = $error;

    if (isset($_SERVER['HTTP_REFERER'])) {
        header('Location: '.$_SERVER['HTTP_REFERER']);
        echo "Error: ".$error;
        die();
    }

}

function redirectSuccess()
{
    $_SESSION['_contact_form_success'] = true;
    if (isset($_SERVER['HTTP_REFERER'])) {
        header('Location: '.$_SERVER['HTTP_REFERER']);
        echo "Your message was sent successfully!";
        die();
    }
    
}
