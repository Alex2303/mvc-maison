<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
        <link rel="stylesheet" type="text/css" href="<?= SCRIPT_ROOT . 'css/style.css'; ?>">
        <!-- bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
        <!-- fontawesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" />
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <!-- Recaptcha -->
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <title>Edelweiss magazine</title>
    </head>
    <body>
        <header class="clear">
            <div class="site-branding">
                <a class="removelinkdefault" href="<?php echo SCRIPT_ROOT;?>"><h4 class="site-title">Edelweiss Magazine</h4></a>
            </div>
            <nav id="site-navigation" class="main-nav">
                <ul class="menu-list">
                    <a class="removelinkdefault" href="<?php echo SCRIPT_ROOT;?>"><h4 class="menu-list__items strike">Home</h4></a>
                    <a class="removelinkdefault" href="<?php echo SCRIPT_ROOT;?>"><h4 class="menu-list__items strike">About us</h4></a>
                    <a class="removelinkdefault" href="<?php echo SCRIPT_ROOT.'/contact/email_me';?>"><h4 class="menu-list__items strike">Contact</h4></a>
                    <?php 
                        // Si l'utilisateur est connecté, on affiche le lien pour créer un article + son nom d'utilisateur
                        if(isLogged()) {
                    ?>
                            <a class="removelinkdefault" href="<?php echo SCRIPT_ROOT.'/articles/add';?>"><h4 class="menu-list__items strike">Create an article</h4></a>  
                            <h4 class="menu-list__items"><i class="fas fa-user-alt"></i><?php echo $_SESSION['pseudo']?> | <a href="<?php echo SCRIPT_ROOT.'/securities/logout'; ?>">Log out</a></h4>
                    <?php
                        }
                    ?>
                   
                </ul>
            </nav>
            <div id="burgerBtn" class="burger">
                <span></span>
            </div>
        </header>
        <!--On lit le contenu de la variable content si elle existe-->
        <?php 
        $content = isset($content) ? $content : '';
        echo $content;
        ?>
        <div class="space">
        <footer>
            <span>© Hugo Darraïdou</span>
        </footer>
        <script type="text/javascript" src="<?= SCRIPT_ROOT . 'script/script.js'; ?>"></script>
    </body>
</html>
