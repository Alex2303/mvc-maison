<!-- Ajout / modification d'un article -->

<div class="container">
    <form action="" method="POST" enctype = "multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $current_photo['id'] ?? 0; ?>">

        <label for="title">Title</label>
        <input type="text" id ="title" name="title" class="form-control" value="<?php echo $current_photo['title'] ?? ''; ?>" />

        <label for="content">Content</label>
        <input type="text" id ="content" name="content" class="form-control" value="<?php echo $current_photo['content'] ?? ''; ?>" />

        <label for="picture">Image</label>
        <input type="file" name="picture" id ="picture" class="form-control" value = "<?php echo $current_photo['picture'] ?? ''; ?>" />

        <input class="btn btn-primary btn-submit" type="submit" value="Valider" />
    </form>
    <a href="<?php echo SCRIPT_ROOT?>">Back to home</a>
</div>