<section class="gallery">
<?php 
    $articleLength = count($articles); //on compte la longueur du tableau articles
    $count = 0; ?> <!-- initialisation d'un compteur à 0 -->
    <?php while($count < $articleLength): ?> <!-- tant que le compteur n'a pas bouclé sur tous les articles -->

        <div class="wrap-img">
            <img class="items-gallery"
                src="<?php echo htmlspecialchars(SCRIPT_ROOT.'/'.$articles[$count]['picture'], ENT_QUOTES); ?>" 
                alt="<?php echo htmlspecialchars($articles[$count]['title'], ENT_QUOTES);?>" 
            />
            <p class="img-title"><?php echo htmlspecialchars($articles[$count]['title'], ENT_QUOTES); ?></p>
            <?php 
                if(isLogged()) {
            ?>  
                    <a class="action-icons" href="<?php echo htmlspecialchars(SCRIPT_ROOT.'/articles/edit/?id='.$articles[$count]['id'], ENT_QUOTES); ?>"><i class="fas fa-pencil-alt"></i></a>
                    <a class="action-icons" href="<?php echo htmlspecialchars(SCRIPT_ROOT.'/articles/remove/?id='.$articles[$count]['id'], ENT_QUOTES); ?>"><i class="fa fa-trash"></i></a>
            <?php
                }
            ?>
        </div>

        <?php 
  
        $count = $count + 1;
        endwhile; 
        ?>

</section>

<!-- Au clique sur la photo, un carousel s'affiche -->
<div id="modal">
    <span id="closeModal"><i class="fas fa-times"></i></span>
    <img class="modal-content" id="img01" />
    <div id="caption" class="modal-title"></div>
    <i class="fas fa-chevron-left"></i>
    <i class="fas fa-chevron-right"></i>
</div>



