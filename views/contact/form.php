<?php

if (!empty($_SESSION['_contact_form_error'])) {
    $error = $_SESSION['_contact_form_error'];
    unset($_SESSION['_contact_form_error']);
}

if (!empty($_SESSION['_contact_form_success'])) {
    $success = true;
    unset($_SESSION['_contact_form_success']);
}

if (!empty($success)) {
    ?>
    <div class="alert alert-success">Your message was sent successfully!</div>
    <?php
}
?>

<?php
if (!empty($error)) {
    ?>
    <div class="alert alert-danger"><?= $error ?></div>
    <?php
}
?>
<div class="container">
    <form action="" method="POST">

        <label for="name">Your name</label>
        <input type="text" id ="name" name="name" class="form-control" />

        <label for="email">Your email</label>
        <input type="email" id ="email" name="email" class="form-control" />

        <label for="subject">Your subject</label>
        <input type="text" id ="subject" name="subject" class="form-control" />

        <label for="message">Your Message</label>
        <textarea name="message" id ="message" class="form-control" rows="5" cols="33"></textarea>

        <div class="form-group text-center">
            <div class="g-recaptcha" data-sitekey="<?= CONTACTFORM_RECAPTCHA_SITE_KEY ?>"></div>
        </div>

        <input class="btn btn-primary btn-submit" type="submit" value="Valider" />
    </form>
    <a href="<?php echo SCRIPT_ROOT?>">Back to home</a>
</div>