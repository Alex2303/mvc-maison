<?php

session_start();

//On réecrit les urls selon le modèle /controller/action/param
//de cette façon, on va pouvoir contrôler la redirection. Selon, la requête, le bon controlleur est appelé avec sa méthode et son paramètre 

require_once __DIR__.'/functions.php';

require_once __DIR__.'/config.php';

// captcha
require_once __DIR__.'/lib/google/recaptcha/src/autoload.php';


// phpmailer
require_once __DIR__.'/lib/phpmailer/phpmailer/src/Exception.php';
require_once __DIR__.'/lib/phpmailer/phpmailer/src/PHPMailer.php';
require_once __DIR__.'/lib/phpmailer/phpmailer/src/SMTP.php';

require_once __DIR__.'/models/Model.php';
require_once __DIR__.'/models/Controller.php';

// On formate l'url en le séparant par des "/"
$url = explode('/', $_GET['url']);

// Si le premier paramètre n'est pas vide
if($url[0] !== ''){
    // Si il existe un fichier comportant comme nom ce qu'il y a en premier paramètre
    if(file_exists('controllers/'.ucfirst($url[0]).'.php')){
        // alors on définit le controller avec ce com
        $controller = ucfirst($url[0]);
        // Si il y a un deuxième paramètre, alors on le définit sinon on définit la page d'accueil
        $action = isset($url[1]) ? $url[1] : 'index'; 
        // On inclue le controller spécifique
        require_once ROOT.'controllers/'.$controller.'.php';
        // On instancie un nouvel objet de la classe controller
        $controller = new $controller();
        // Si le controller définit comporte une méthode
        if(method_exists($controller, $action)){
            // On détruit le premier et le deuxième paramètre du tableau
            unset($url[0]);
            unset($url[1]);
            // et on reformate l'url afin d'y insérer un paramètre "$url", call_user_func_array([$controller, $action] ..) fait la même chose que $controller->$action
            call_user_func_array([$controller, $action], $url);
            
        }else{
            // Sinon on on renvoie un code page not found
            http_response_code(404);
            echo "la page demandée n'existe pas !";
        }
    }else{
        http_response_code(404);
        echo "la page demandée n'existe pas !";
    }
}else{
    // Sinon on inclue le controller Articles
    require_once ROOT.'controllers/Articles.php';
    // On instancie un nouvel objet de la classe Articles
    $controller = new Articles();
    // On appelle la méthode index pour afficher la vue
    $controller->index();
}

