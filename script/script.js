var burger = document.getElementById('burgerBtn');
var menu = document.getElementById('site-navigation');
var responsiveMenuWrap = document.getElementById('menu-wrap');

burger.addEventListener('click', menuOpen);

function menuOpen() 
{
    burger.classList.toggle('active');
    menu.classList.toggle('open');
}

//Si je suis en responsive, mon menu va dans la balise menu-wrap
if (window.matchMedia("(max-width: 900px)").matches) {
    menu.classList.toggle('main-nav-responsive.open');
}


//Au click sur une image, un modal s'ouvre avec l'image et le texte 
let images = document.querySelectorAll('img.items-gallery');
let modal = document.querySelector("#modal");
let closeModal = document.querySelector("#closeModal");
let captionText = document.querySelector("#caption");
let modalImg = document.querySelector("#img01");

images.forEach(image => {
    image.addEventListener('click', function() {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    });
});

//ferme le modal
closeModal.addEventListener('click', function() {
    modal.style.display = "none";
}); 

//Au click sur une des flèches, l'image suivante/précédente s'affiche
let btnLeft = document.querySelector('.fas.fa-chevron-left');
let btnRight = document.querySelector('.fas.fa-chevron-right');

btnRight.addEventListener('click', function() {
    changeSlide(1);//0 + 1 = 1 -> + 1 = 2 ..
});

btnLeft.addEventListener('click', function() {
    changeSlide(-1); // 0 - 1 = 0 donc < 0 -> dernière image 5 - 1 = 4 -> 4 - 1 = 3 ..
});

let numero = 0;
function changeSlide(sens) {
    numero = numero + sens;
    if(numero <0) {
        numero = images.length - 1; //affiche la dernière image si on clique sur précédent depuis la première image
    }
    if(numero > images.length - 1) {
        numero = 0; // affiche la première image si on clique sur suivant depuis la dernière image
    }
    modalImg.src = images[numero].src;
    captionText.innerHTML = images[numero].alt;
}

// let imgs = document.getElementsByClassName('items-gallery');
// [...imgs].forEach((img) => {
//     console.log(img);
//     img.addEventListener("mouseover", function(event){
//         event.target.classList.toggle('title_hover');
//     });
// })














 

