<?php 

class Securities extends Controller {
    public function login() {
        $log = $this->loadModel('Security');
        $message = '';
        
        if (isset($_POST['pseudo']) && isset($_POST['password'])) {
            $user = $_POST['pseudo'];
            $pass = $_POST['password'];
            $result = $log->userExists($user, $pass);
            // On vérifie qu'il y a un résultat (l'utilisateur existe)
            if ($result->rowCount() == 1) {
                // On enregistre dans une session le nom de l'utilisateur
                session_start();
                $_SESSION['pseudo'] = $user;
                $_SESSION['password'] = $pass;
                //Si oui on retourne sur la page d'accueil
                header('Location: ' .SCRIPT_ROOT);
            } else {
                // On affiche un message d'erreur
                $message .= "<div class='alert alert-danger'>Vérifiez les infos de connexion<div>";
            }
        }
        echo $message;
        $this->render('login', ['log' => $log]); 
    }
    public function logout() {
        session_start();
        session_destroy();
        header('Location: ' .SCRIPT_ROOT);
        exit;
    }
}