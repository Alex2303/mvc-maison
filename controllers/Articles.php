<?php

/**
 * Objet Article héritant des méthodes de la classe controller
 */
class Articles extends Controller{
    public function index(){
        // On inclue le model Article et on instancie un nouvel objet de cette classe via la méthode loadModel de la classe Controller
        $article = $this->loadModel('Article'); 
        // On récupère les articles tous les articles
        $articles = $article->getAll(); 
        // On envoie la variable article à la vue articles/index dans views
        $this->render('index', ['articles' => $articles]); 
    }

    public function add($photo = null){
        if(isLogged()) {
            $message = '';
            $form = '';
            //Si le formulaire n'est pas vide (donc envoyé) et qu'il contient une image
            if (!empty($_POST) && isset($_FILES['picture'])) { 
                // upload :
                $file_name = $_FILES['picture']['name']; //nom du fichier
                $file_size =$_FILES['picture']['size']; //taille
                $file_tmp =$_FILES['picture']['tmp_name']; //chemin du fichier temporaire
                $file_type=$_FILES['picture']['type']; //type
                //On isole l'extension du fichier
                $tmp = explode('.',$_FILES['picture']['name']);
                $file_ext = end($tmp);
                //Les extensions acceptées pour les fichiers
                $extensions = array("jpeg","jpg","png");
                
                //contrôles 
                //vérification $_FILES

                //Si le tableau ne contient pas les extensions mentionnées, on affiche un message d'erreur
                if(in_array($file_ext,$extensions)=== false){
                    $message .= '<div class="alert alert-danger">Extension non prise en compte ! Merci de choisir un fichier JPG, JPEG ou PNG !</div>';
                }
                
                if($_FILES['picture']['error'] == UPLOAD_ERR_INI_SIZE ){
                    $message .= '<div class="alert alert-danger">Le fichier ne peut dépasser 2 MB !</div>';
                }

                // if($_FILES['picture']['error'] == UPLOAD_ERR_INI_SIZE ){
                //     $message .= '<div class="alert alert-danger">Le fichier ne peut dépasser 2 MB !</div>';
                // }
               
                //vérification méthode POST
                if(!isset($_POST['title']) || strlen($_POST['title'])<4 || strlen($_POST['title'])>180) {
                    $message .= '<div class="alert alert-danger">Le titre doit contenir entre 4 et 180 caractères.</div>';
                }
                if(!isset($_POST['content']) || strlen($_POST['content'])<4 || strlen($_POST['content'])>500) {
                    $message .= '<div class="alert alert-danger">Le message doit contenir entre 4 et 500 caractères.</div>';
                }
            
                //On ajoute les infos dans la BDD
                if(empty($message) == true){ //Si il n'y a pas d'erreurs, on déplace le fichier uploadé (stocké dans les fichiers temporaires) dans le dossier upload de l'application
                    $photo = 'uploads/'.uniqid() . '_' . $file_name;
                    move_uploaded_file($file_tmp, $photo); //$photo = destination

                    $article = $this->loadModel('Article');
                    $form = $article->create($photo);

                    // Si il y a un retour (true), alors on affiche un message de succès sinon l'erreur
                    if($form) {
                        $message .= '<div class="alert alert-success">L\'image a bien été ajouté.</div>';
                    } 
                    else {
                        $message .= '<div class="alert alert-danger">Erreur lors de l\'enregistrement.</div>';
                    }
                } 
            }
            // On affiche l'erreur
            echo $message;
            $this->render('form', ['form' => $form]); 
        }
        else {
            header('Location: ' .SCRIPT_ROOT.'/securities/login');
        }
    }
    public function edit($photo = null) {
        if(isLogged() ){
            // Si existe "id" dans l'URL, c'est qu'on a demandé la modification de l'article. On sélectionne alors ses données en BDD pour les mettre dans le formulaire.
            if(isset($_GET['id'])){ 
                $article = $this->loadModel('Article');
                // pré-remplir le formulaire de modification
                $current_photo = $article->getOne(); // "SELECT * FROM ".$this->table." WHERE id = ".$this->id
                $this->render('form', ['current_photo' => $current_photo]);
                // On fait comme pour la méthode add
                $message = '';
                $form = '';

                if (!empty($_POST) && isset($_FILES['picture'])) { 
                    $file_name = $_FILES['picture']['name']; //nom du fichier
                    $file_size =$_FILES['picture']['size']; //taille
                    $file_tmp =$_FILES['picture']['tmp_name']; //chemin du fichier temporaire
                    $file_type=$_FILES['picture']['type']; //type
                    $tmp = explode('.',$_FILES['picture']['name']);
                    $file_ext = end($tmp);
                    $extensions = array("jpeg","jpg","png");

                    if(in_array($file_ext,$extensions)=== false){
                        $message .= '<div class="alert alert-danger">Extension non prise en compte ! Merci de choisir un fichier JPG, JPEG ou PNG !</div>';
                    }                   
                    
                    if($_FILES['picture']['error'] == UPLOAD_ERR_INI_SIZE ){
                        $message .= '<div class="alert alert-danger">Le fichier ne peut dépasser 2 MB !</div>';
                    }

                    // if($_FILES['picture']['error'] == UPLOAD_ERR_INI_SIZE ){
                    //     $message .= '<div class="alert alert-danger">Le fichier ne peut dépasser 2 MB !</div>';
                    // }
    
                    if(!isset($_POST['title']) || strlen($_POST['title'])<4 || strlen($_POST['title'])>180) {
                        $message .= '<div class="alert alert-danger">Le titre doit contenir entre 4 et 180 caractères.</div>';
                    }
                    if(!isset($_POST['content']) || strlen($_POST['content'])<4 || strlen($_POST['content'])>500) {
                        $message .= '<div class="alert alert-danger">Le message doit contenir entre 4 et 500 caractères.</div>';
                    }

                    if(empty($message) == true){ //Si il n'y a pas d'erreurs, on déplace le fichier uploadé (stocké dans les fichiers temporaires) dans le dossier upload de l'application
                        $photo = 'uploads/'.uniqid() . '_' . $file_name;
                        move_uploaded_file($file_tmp, $photo); //$photo = destination

                        $current_photo = $article->create($photo);

                        if($current_photo) {
                            $message .= '<div class="alert alert-success">L\'image a bien été modifiée.</div>';
                        } 
                        else {
                            $message .= '<div class="alert alert-danger">Erreur lors de l\'enregistrement.</div>';
                        }
                    } 
                }
                echo $message;
                $this->render('form', ['current_photo' => $current_photo]); 
            }
            else {
                header('Location: ' .SCRIPT_ROOT.'/securities/login');
            }
        }
    }
    public function remove() {
        // On charge le modèle Article avec le méthode loadModel de Controller
        $article = $this->loadModel('Article');
        // On cherche l'article
        $articleToDelete = $article->getOne();
        // On supprimer le fichier du dossier upload
        unlink($articleToDelete['picture']);
        // On appelle la méthode remove qui supprime l'article
        $result = $article->remove();
        // On retourne à l'accueil
        header('Location: ' .SCRIPT_ROOT);
    }
}